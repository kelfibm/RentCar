﻿using System.Collections.Generic;

namespace RentCar.Model
{
    public class TipoPersona
    {
        public int IdTipoPersona { get; set; }
        public string Descripcion { get; set; }
        public bool Estado { get; set; } = true;

        public virtual List<Cliente> Clientes { get; set; }
    }
}
