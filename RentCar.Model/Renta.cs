﻿using System;

namespace RentCar.Model
{
    public class Renta
    {
        public int IdRenta { get; set; }
        public DateTime FechaRenta { get; set; }
        public DateTime? FechaDevolucion { get; set; }
        public decimal MontoDia { get; set; }
        public int CantidadDias { get; set; }
        public string Comentario { get; set; }
        public bool Estado { get; set; } = true;

        public int IdEmpleado { get; set; }
        public virtual Empleado Empleado { get; set; }

        public int IdVehiculo { get; set; }
        public virtual Vehiculo Vehiculo { get; set; }

        public int IdCliente { get; set; }
        public virtual Cliente Cliente { get; set; }
    }
}
