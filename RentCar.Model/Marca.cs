﻿using System.Collections.Generic;

namespace RentCar.Model
{
    public class Marca
    {
        public int IdMarca { get; set; }
        public string Descripcion { get; set; }
        public bool Estado { get; set; } = true;

        public virtual List<Modelo> Modelos { get; set; }
    }
}
