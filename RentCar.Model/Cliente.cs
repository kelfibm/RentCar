﻿using System.Collections.Generic;

namespace RentCar.Model
{
    public class Cliente
    {
        public int IdCliente { get; set; }
        public string Nombre { get; set; }
        public string Cedula { get; set; }
        public string NoTarjeta { get; set; }
        public decimal LimiteCredito { get; set; }
        public bool Estado { get; set; } = true;

        public int IdTipoPersona { get; set; }
        public virtual TipoPersona TipoPersona { get; set; }

        public virtual List<Inspeccion> Inspecciones { get; set; }
        public virtual List<Renta> Rentas { get; set; }
    }
}
