﻿using System.Collections.Generic;

namespace RentCar.Model
{
    public class Modelo
    {
        public int IdModelo { get; set; }
        public string Descripcion { get; set; }
        public bool Estado { get; set; } = true;

        public int IdMarca { get; set; }
        public virtual Marca Marca { get; set; }

        public virtual List<Vehiculo> Vehiculos { get; set; }
    }
}
