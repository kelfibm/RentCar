﻿using System.Collections.Generic;

namespace RentCar.Model
{
    public class Vehiculo
    {
        public int IdVehiculo { get; set; }
        public string Descripcion { get; set; }
        public string NoChasis { get; set; }
        public string NoMotor { get; set; }
        public string NoPlaca { get; set; }
        public bool Inspeccionado { get; set; }
        public bool Estado { get; set; } = true;

        public int IdTipoVehiculo { get; set; }
        public virtual TipoVehiculo TipoVehiculo { get; set; }

        public int IdModelo { get; set; }
        public virtual Modelo Modelo { get; set; }
        
        public int IdTipoCombustible { get; set; }
        public virtual TipoCombustible TipoCombustible { get; set; }

        public virtual List<Renta> Rentas { get; set; }
        public virtual List<Inspeccion> Inspecciones { get; set; }
    }
}
