﻿using System.Collections.Generic;

namespace RentCar.Model
{
    public class TipoCombustible
    {
        public int IdTipoCombustible { get; set; }
        public string Descripcion { get; set; }
        public bool Estado { get; set; } = true;

        public virtual List<Vehiculo> Vehiculos { get; set; }
    }
}
