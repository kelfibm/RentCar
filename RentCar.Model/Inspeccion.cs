﻿using System;

namespace RentCar.Model
{
    public class Inspeccion
    {
        public int IdInspeccion { get; set; }
        public bool Ralladuras { get; set; }
        public double CantidadCombustible { get; set; }
        public bool GomaRespuesto { get; set; }
        public bool Gato { get; set; }
        public bool Roturas { get; set; }
        public int GomasEstables { get; set; }
        public string Observaciones { get; set; }
        public DateTime Fecha { get; set; }
        public bool Estado { get; set; } = true;

        public int IdVehiculo { get; set; }
        public virtual Vehiculo Vehiculo { get; set; }
        
        public int IdCliente { get; set; }
        public virtual Cliente Cliente { get; set; }
        
        public int IdEmpleado { get; set; }
        public virtual Empleado Empleado { get; set; }
    }
}
