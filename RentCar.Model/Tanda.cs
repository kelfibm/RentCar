﻿using System.Collections.Generic;

namespace RentCar.Model
{
    public class Tanda
    {
        public int IdTanda { get; set; }
        public string Descripcion { get; set; }
        public bool Estado { get; set; } = true;

        public virtual List<Empleado> Empleados { get; set; }
    }
}
