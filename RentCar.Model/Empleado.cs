﻿using System;
using System.Collections.Generic;

namespace RentCar.Model
{
    public class Empleado
    {
        public int IdEmpleado { get; set; }
        public string Nombre { get; set; }
        public string Cedula { get; set; }
        public decimal Comision { get; set; }
        public DateTime FechaIngreso { get; set; }
        public bool Estado { get; set; } = true;

        public int IdTanda { get; set; }
        public virtual Tanda Tanda { get; set; }
        
        public virtual List<Renta> Rentas { get; set; }
        public virtual List<Inspeccion> Inspecciones { get; set; }

    }
}
