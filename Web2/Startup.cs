﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RentCar.Model;
using Web2.Data;
using Web2.Models;
using Web2.Services;

namespace Web2
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            // Add application services.
            services.AddTransient<IEmailSender, EmailSender>();

            services.AddDbContext<RentCarDbContext>();

            services.AddMvc().AddFluentValidation();
            services.AddTransient<IValidator<Cliente>, ClienteValidator>();
            services.AddTransient<IValidator<Empleado>, EmpleadoValidator>();
            services.AddTransient<IValidator<Inspeccion>, InspeccionValidator>();
            services.AddTransient<IValidator<Marca>, MarcaValidator>();
            services.AddTransient<IValidator<Modelo>, ModeloValidator>();
            services.AddTransient<IValidator<Renta>, RentaValidator>();
            services.AddTransient<IValidator<Tanda>, TandaValidator>();
            services.AddTransient<IValidator<TipoCombustible>, TipoCombustibleValidator>();
            services.AddTransient<IValidator<TipoPersona>, TipoPersonaValidator>();
            services.AddTransient<IValidator<TipoVehiculo>, TipoVehiculoValidator>();
            services.AddTransient<IValidator<Vehiculo>, VehiculoValidator>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
