﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RentCar.Model;
using Web2.Data;

namespace Web2.Controllers
{
    public class TipoCombustiblesController : Controller
    {
        private readonly RentCarDbContext _context;

        public TipoCombustiblesController(RentCarDbContext context)
        {
            _context = context;
        }

        // GET: TipoCombustibles
        public async Task<IActionResult> Index()
        {
            return View(await _context.TipoCombustibles.Where(m => m.Estado).ToListAsync());
        }

        // GET: TipoCombustibles/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipoCombustible = await _context.TipoCombustibles
                .SingleOrDefaultAsync(m => m.IdTipoCombustible == id && m.Estado);
            if (tipoCombustible == null)
            {
                return NotFound();
            }

            return View(tipoCombustible);
        }

        // GET: TipoCombustibles/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: TipoCombustibles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdTipoCombustible,Descripcion")] TipoCombustible tipoCombustible)
        {
            if (!ModelState.IsValid) return View(tipoCombustible);
            _context.Add(tipoCombustible);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        // GET: TipoCombustibles/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipoCombustible = await _context.TipoCombustibles.SingleOrDefaultAsync(m => m.IdTipoCombustible == id && m.Estado);
            if (tipoCombustible == null)
            {
                return NotFound();
            }
            return View(tipoCombustible);
        }

        // POST: TipoCombustibles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdTipoCombustible,Descripcion")] TipoCombustible tipoCombustible)
        {
            if (id != tipoCombustible.IdTipoCombustible)
            {
                return NotFound();
            }

            if (!ModelState.IsValid) return View(tipoCombustible);
            try
            {
                _context.Update(tipoCombustible);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TipoCombustibleExists(tipoCombustible.IdTipoCombustible))
                {
                    return NotFound();
                }
                throw;
            }
            return RedirectToAction(nameof(Index));
        }

        // GET: TipoCombustibles/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipoCombustible = await _context.TipoCombustibles
                .SingleOrDefaultAsync(m => m.IdTipoCombustible == id && m.Estado);
            if (tipoCombustible == null)
            {
                return NotFound();
            }

            return View(tipoCombustible);
        }

        // POST: TipoCombustibles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tipoCombustible = await _context.TipoCombustibles.SingleOrDefaultAsync(m => m.IdTipoCombustible == id);
            tipoCombustible.Estado = false;
            _context.Update(tipoCombustible);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TipoCombustibleExists(int id)
        {
            return _context.TipoCombustibles.Any(e => e.IdTipoCombustible == id && e.Estado);
        }
    }
}
