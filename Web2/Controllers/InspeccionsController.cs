﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RentCar.Model;
using Web2.Data;

namespace Web2.Controllers
{
    public class InspeccionsController : Controller
    {
        private readonly RentCarDbContext _context;

        public InspeccionsController(RentCarDbContext context)
        {
            _context = context;
        }

        // GET: Inspeccions
        public async Task<IActionResult> Index() 
        {
            var rentCarDbContext = _context.Inspecciones.Include(i => i.Cliente).Include(i => i.Empleado).Include(i => i.Vehiculo).Where(i => i.Estado);
            return View(await rentCarDbContext.ToListAsync());
        }

        // GET: Inspeccions/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var inspeccion = await _context.Inspecciones
                .Include(i => i.Cliente)
                .Include(i => i.Empleado)
                .Include(i => i.Vehiculo)
                .SingleOrDefaultAsync(m => m.IdInspeccion == id && m.Estado);
            if (inspeccion == null)
            {
                return NotFound();
            }

            return View(inspeccion);
        }

        // GET: Inspeccions/Create
        public IActionResult Create()
        {
            ViewData["IdCliente"] = new SelectList(_context.Clientes, "IdCliente", "Cedula");
            ViewData["IdEmpleado"] = new SelectList(_context.Empleados, "IdEmpleado", "Cedula");
            ViewData["IdVehiculo"] = new SelectList(_context.Vehiculos, "IdVehiculo", "Descripcion");
            return View();
        }

        // POST: Inspeccions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdInspeccion,Ralladuras,CantidadCombustible,GomaRespuesto,Gato,Roturas,GomasEstables,Observaciones,IdVehiculo,IdCliente,IdEmpleado")] Inspeccion inspeccion)
        {
            inspeccion.Fecha = DateTime.Now;
            if (ModelState.IsValid)
            {
                _context.Add(inspeccion);
                var vehiculo = _context.Vehiculos.FirstOrDefault(m => m.IdVehiculo == inspeccion.IdVehiculo && m.Estado);
                if (vehiculo != null)
                {
                    vehiculo.Inspeccionado = true;
                    _context.Update(vehiculo);
                }
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdCliente"] = new SelectList(_context.Clientes, "IdCliente", "Cedula", inspeccion.IdCliente);
            ViewData["IdEmpleado"] = new SelectList(_context.Empleados, "IdEmpleado", "Cedula", inspeccion.IdEmpleado);
            ViewData["IdVehiculo"] = new SelectList(_context.Vehiculos, "IdVehiculo", "NoPlaca", inspeccion.IdVehiculo);
            return View(inspeccion);
        }

        // GET: Inspeccions/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var inspeccion = await _context.Inspecciones.SingleOrDefaultAsync(m => m.IdInspeccion == id && m.Estado);
            if (inspeccion == null)
            {
                return NotFound();
            }
            ViewData["IdCliente"] = new SelectList(_context.Clientes, "IdCliente", "Cedula", inspeccion.IdCliente);
            ViewData["IdEmpleado"] = new SelectList(_context.Empleados, "IdEmpleado", "Cedula", inspeccion.IdEmpleado);
            ViewData["IdVehiculo"] = new SelectList(_context.Vehiculos, "IdVehiculo", "Descripcion", inspeccion.IdVehiculo);
            return View(inspeccion);
        }

        // POST: Inspeccions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdInspeccion,Ralladuras,CantidadCombustible,GomaRespuesto,Gato,Roturas,GomasEstables,Observaciones,IdVehiculo,IdCliente,IdEmpleado")] Inspeccion inspeccion)
        {
            if (id != inspeccion.IdInspeccion)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(inspeccion);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!InspeccionExists(inspeccion.IdInspeccion))
                    {
                        return NotFound();
                    }

                    throw;
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdCliente"] = new SelectList(_context.Clientes, "IdCliente", "Cedula", inspeccion.IdCliente);
            ViewData["IdEmpleado"] = new SelectList(_context.Empleados, "IdEmpleado", "Cedula", inspeccion.IdEmpleado);
            ViewData["IdVehiculo"] = new SelectList(_context.Vehiculos, "IdVehiculo", "Descripcion", inspeccion.IdVehiculo);
            return View(inspeccion);
        }

        // GET: Inspeccions/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var inspeccion = await _context.Inspecciones
                .Include(i => i.Cliente)
                .Include(i => i.Empleado)
                .Include(i => i.Vehiculo)
                .SingleOrDefaultAsync(m => m.IdInspeccion == id && m.Estado);
            if (inspeccion == null)
            {
                return NotFound();
            }

            return View(inspeccion);
        }

        // POST: Inspeccions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var inspeccion = await _context.Inspecciones.SingleOrDefaultAsync(m => m.IdInspeccion == id && m.Estado);
            inspeccion.Estado = false;
            _context.Update(inspeccion);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool InspeccionExists(int id)
        {
            return _context.Inspecciones.Any(e => e.IdInspeccion == id && e.Estado);
        }
    }
}
