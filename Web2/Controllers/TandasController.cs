﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RentCar.Model;
using Web2.Data;

namespace Web2.Controllers
{
    public class TandasController : Controller
    {
        private readonly RentCarDbContext _context;

        public TandasController(RentCarDbContext context)
        {
            _context = context;
        }

        // GET: Tandas
        public async Task<IActionResult> Index()
        {
            return View(await _context.Tandas.Where(m => m.Estado).ToListAsync());
        }

        // GET: Tandas/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tanda = await _context.Tandas
                .SingleOrDefaultAsync(m => m.IdTanda == id && m.Estado);
            if (tanda == null)
            {
                return NotFound();
            }

            return View(tanda);
        }

        // GET: Tandas/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Tandas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdTanda,Descripcion")] Tanda tanda)
        {
            if (!ModelState.IsValid) return View(tanda);
            _context.Add(tanda);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        // GET: Tandas/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tanda = await _context.Tandas.SingleOrDefaultAsync(m => m.IdTanda == id && m.Estado);
            if (tanda == null)
            {
                return NotFound();
            }
            return View(tanda);
        }

        // POST: Tandas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdTanda,Descripcion")] Tanda tanda)
        {
            if (id != tanda.IdTanda)
            {
                return NotFound();
            }

            if (!ModelState.IsValid) return View(tanda);
            try
            {
                _context.Update(tanda);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TandaExists(tanda.IdTanda))
                {
                    return NotFound();
                }

                throw;
            }
            return RedirectToAction(nameof(Index));
        }

        // GET: Tandas/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tanda = await _context.Tandas
                .SingleOrDefaultAsync(m => m.IdTanda == id && m.Estado);
            if (tanda == null)
            {
                return NotFound();
            }

            return View(tanda);
        }

        // POST: Tandas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tanda = await _context.Tandas.SingleOrDefaultAsync(m => m.IdTanda == id && m.Estado);
            tanda.Estado = false;
            _context.Update(tanda);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TandaExists(int id)
        {
            return _context.Tandas.Any(e => e.IdTanda == id && e.Estado);
        }
    }
}
