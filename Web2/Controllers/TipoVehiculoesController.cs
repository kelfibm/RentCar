﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RentCar.Model;
using Web2.Data;

namespace Web2.Controllers
{
    public class TipoVehiculoesController : Controller
    {
        private readonly RentCarDbContext _context;

        public TipoVehiculoesController(RentCarDbContext context)
        {
            _context = context;
        }

        // GET: TipoVehiculoes
        public async Task<IActionResult> Index()
        {
            return View(await _context.TipoVehiculos.Where(m => m.Estado).ToListAsync());
        }

        // GET: TipoVehiculoes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipoVehiculo = await _context.TipoVehiculos
                .SingleOrDefaultAsync(m => m.IdTipoVehiculo == id && m.Estado);
            if (tipoVehiculo == null)
            {
                return NotFound();
            }

            return View(tipoVehiculo);
        }

        // GET: TipoVehiculoes/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: TipoVehiculoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdTipoVehiculo,Descripcion")] TipoVehiculo tipoVehiculo)
        {
            if (!ModelState.IsValid) return View(tipoVehiculo);
            _context.Add(tipoVehiculo);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        // GET: TipoVehiculoes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipoVehiculo = await _context.TipoVehiculos.SingleOrDefaultAsync(m => m.IdTipoVehiculo == id && m.Estado);
            if (tipoVehiculo == null)
            {
                return NotFound();
            }
            return View(tipoVehiculo);
        }

        // POST: TipoVehiculoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdTipoVehiculo,Descripcion")] TipoVehiculo tipoVehiculo)
        {
            if (id != tipoVehiculo.IdTipoVehiculo)
            {
                return NotFound();
            }

            if (!ModelState.IsValid) return View(tipoVehiculo);
            try
            {
                _context.Update(tipoVehiculo);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TipoVehiculoExists(tipoVehiculo.IdTipoVehiculo))
                {
                    return NotFound();
                }
                throw;
            }
            return RedirectToAction(nameof(Index));
        }

        // GET: TipoVehiculoes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipoVehiculo = await _context.TipoVehiculos
                .SingleOrDefaultAsync(m => m.IdTipoVehiculo == id && m.Estado);
            if (tipoVehiculo == null)
            {
                return NotFound();
            }

            return View(tipoVehiculo);
        }

        // POST: TipoVehiculoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tipoVehiculo = await _context.TipoVehiculos.SingleOrDefaultAsync(m => m.IdTipoVehiculo == id);
            tipoVehiculo.Estado = false;
            _context.Update(tipoVehiculo);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TipoVehiculoExists(int id)
        {
            return _context.TipoVehiculos.Any(e => e.IdTipoVehiculo == id && e.Estado);
        }
    }
}
