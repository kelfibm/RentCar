﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RentCar.Model;
using Web2.Data;

namespace Web2.Controllers
{
    public class VehiculoesController : Controller
    {
        private readonly RentCarDbContext _context;

        public VehiculoesController(RentCarDbContext context)
        {
            _context = context;
        }

        // GET: Vehiculoes
        public async Task<IActionResult> Index()
        {
            var rentCarDbContext = _context.Vehiculos.Include(v => v.Modelo).Include(v => v.TipoCombustible).Include(v => v.TipoVehiculo).Where(m => m.Estado);
            return View(await rentCarDbContext.ToListAsync());
        }

        // GET: Vehiculoes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var vehiculo = await _context.Vehiculos
                .Include(v => v.Modelo)
                .Include(v => v.TipoCombustible)
                .Include(v => v.TipoVehiculo)
                .SingleOrDefaultAsync(m => m.IdVehiculo == id && m.Estado);
            if (vehiculo == null)
            {
                return NotFound();
            }

            return View(vehiculo);
        }

        // GET: Vehiculoes/Create
        public IActionResult Create()
        {
            ViewData["IdModelo"] = new SelectList(_context.Modelos, "IdModelo", "Descripcion");
            ViewData["IdTipoCombustible"] = new SelectList(_context.TipoCombustibles, "IdTipoCombustible", "Descripcion");
            ViewData["IdTipoVehiculo"] = new SelectList(_context.TipoVehiculos, "IdTipoVehiculo", "Descripcion");
            return View();
        }

        // POST: Vehiculoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdVehiculo,Descripcion,NoChasis,NoMotor,NoPlaca,IdTipoVehiculo,IdModelo,IdTipoCombustible")] Vehiculo vehiculo)
        {
            if (ModelState.IsValid)
            {
                _context.Add(vehiculo);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdModelo"] = new SelectList(_context.Modelos, "IdModelo", "Descripcion", vehiculo.IdModelo);
            ViewData["IdTipoCombustible"] = new SelectList(_context.TipoCombustibles, "IdTipoCombustible", "Descripcion", vehiculo.IdTipoCombustible);
            ViewData["IdTipoVehiculo"] = new SelectList(_context.TipoVehiculos, "IdTipoVehiculo", "Descripcion", vehiculo.IdTipoVehiculo);
            return View(vehiculo);
        }

        // GET: Vehiculoes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var vehiculo = await _context.Vehiculos.SingleOrDefaultAsync(m => m.IdVehiculo == id && m.Estado);
            if (vehiculo == null)
            {
                return NotFound();
            }
            ViewData["IdModelo"] = new SelectList(_context.Modelos, "IdModelo", "Descripcion", vehiculo.IdModelo);
            ViewData["IdTipoCombustible"] = new SelectList(_context.TipoCombustibles, "IdTipoCombustible", "Descripcion", vehiculo.IdTipoCombustible);
            ViewData["IdTipoVehiculo"] = new SelectList(_context.TipoVehiculos, "IdTipoVehiculo", "Descripcion", vehiculo.IdTipoVehiculo);
            return View(vehiculo);
        }

        // POST: Vehiculoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdVehiculo,Descripcion,NoChasis,NoMotor,NoPlaca,IdTipoVehiculo,IdModelo,IdTipoCombustible")] Vehiculo vehiculo)
        {
            if (id != vehiculo.IdVehiculo)
            {
                return NotFound();
            }

            if (!ModelState.IsValid)
            {
                ViewData["IdModelo"] = new SelectList(_context.Modelos, "IdModelo", "Descripcion", vehiculo.IdModelo);
                ViewData["IdTipoCombustible"] = new SelectList(_context.TipoCombustibles, "IdTipoCombustible",
                    "Descripcion", vehiculo.IdTipoCombustible);
                ViewData["IdTipoVehiculo"] = new SelectList(_context.TipoVehiculos, "IdTipoVehiculo", "Descripcion",
                    vehiculo.IdTipoVehiculo);
                return View(vehiculo);
            }

            try
            {
                _context.Update(vehiculo);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VehiculoExists(vehiculo.IdVehiculo))
                {
                    return NotFound();
                }
                throw;
            }
            return RedirectToAction(nameof(Index));
        }

        // GET: Vehiculoes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var vehiculo = await _context.Vehiculos
                .Include(v => v.Modelo)
                .Include(v => v.TipoCombustible)
                .Include(v => v.TipoVehiculo)
                .SingleOrDefaultAsync(m => m.IdVehiculo == id && m.Estado);
            if (vehiculo == null)
            {
                return NotFound();
            }

            return View(vehiculo);
        }

        // POST: Vehiculoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var vehiculo = await _context.Vehiculos.SingleOrDefaultAsync(m => m.IdVehiculo == id);
            vehiculo.Estado = false;
            _context.Update(vehiculo);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool VehiculoExists(int id)
        {
            return _context.Vehiculos.Any(e => e.IdVehiculo == id && e.Estado);
        }
    }
}
