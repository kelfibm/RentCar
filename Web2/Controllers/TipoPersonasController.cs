﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RentCar.Model;
using Web2.Data;

namespace Web2.Controllers
{
    public class TipoPersonasController : Controller
    {
        private readonly RentCarDbContext _context;

        public TipoPersonasController(RentCarDbContext context)
        {
            _context = context;
        }

        // GET: TipoPersonas
        public async Task<IActionResult> Index()
        {
            return View(await _context.TipoPersonas.Where(m => m.Estado).ToListAsync());
        }

        // GET: TipoPersonas/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipoPersona = await _context.TipoPersonas
                .SingleOrDefaultAsync(m => m.IdTipoPersona == id && m.Estado);
            if (tipoPersona == null)
            {
                return NotFound();
            }

            return View(tipoPersona);
        }

        // GET: TipoPersonas/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: TipoPersonas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdTipoPersona,Descripcion")] TipoPersona tipoPersona)
        {
            if (!ModelState.IsValid) return View(tipoPersona);
            _context.Add(tipoPersona);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        // GET: TipoPersonas/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipoPersona = await _context.TipoPersonas.SingleOrDefaultAsync(m => m.IdTipoPersona == id && m.Estado);
            if (tipoPersona == null)
            {
                return NotFound();
            }
            return View(tipoPersona);
        }

        // POST: TipoPersonas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdTipoPersona,Descripcion")] TipoPersona tipoPersona)
        {
            if (id != tipoPersona.IdTipoPersona)
            {
                return NotFound();
            }

            if (!ModelState.IsValid) return View(tipoPersona);
            try
            {
                _context.Update(tipoPersona);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TipoPersonaExists(tipoPersona.IdTipoPersona))
                {
                    return NotFound();
                }

                throw;
            }
            return RedirectToAction(nameof(Index));
        }

        // GET: TipoPersonas/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipoPersona = await _context.TipoPersonas
                .SingleOrDefaultAsync(m => m.IdTipoPersona == id && m.Estado);
            if (tipoPersona == null)
            {
                return NotFound();
            }

            return View(tipoPersona);
        }

        // POST: TipoPersonas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tipoPersona = await _context.TipoPersonas.SingleOrDefaultAsync(m => m.IdTipoPersona == id && m.Estado);
            tipoPersona.Estado = false;
            _context.Update(tipoPersona);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TipoPersonaExists(int id)
        {
            return _context.TipoPersonas.Any(e => e.IdTipoPersona == id && e.Estado);
        }
    }
}
