﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RentCar.Model;
using Web2.Data;

namespace Web2.Controllers
{
    public class RentasController : Controller
    {
        private readonly RentCarDbContext _context;

        public RentasController(RentCarDbContext context)
        {
            _context = context;
        }

        // GET: Rentas
        public async Task<IActionResult> Index()
        {
            var rentCarDbContext = _context.Rentas.Include(r => r.Cliente).Include(r => r.Empleado).Include(r => r.Vehiculo).Where(r => r.Estado);
            return View(await rentCarDbContext.ToListAsync());
        }

        // GET: Rentas/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var renta = await _context.Rentas
                .Include(r => r.Cliente)
                .Include(r => r.Empleado)
                .Include(r => r.Vehiculo)
                .SingleOrDefaultAsync(m => m.IdRenta == id && m.Estado);
            if (renta == null)
            {
                return NotFound();
            }

            return View(renta);
        }

        // GET: Rentas/Create
        public IActionResult Create()
        {
            ViewData["IdCliente"] = new SelectList(_context.Clientes, "IdCliente", "Cedula");
            ViewData["IdEmpleado"] = new SelectList(_context.Empleados, "IdEmpleado", "Cedula");
            ViewData["IdVehiculo"] = new SelectList(_context.Vehiculos, "IdVehiculo", "Descripcion");
            return View();
        }

        // POST: Rentas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdRenta,FechaDevolucion,MontoDia,CantidadDias,Comentario,IdEmpleado,IdVehiculo,IdCliente")] Renta renta)
        {
            if (ModelState.IsValid)
            {
                _context.Add(renta);
                var vehiculo = _context.Vehiculos.Find(renta.IdVehiculo);
                vehiculo.Inspeccionado = false;
                _context.Update(vehiculo);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdCliente"] = new SelectList(_context.Clientes, "IdCliente", "Cedula", renta.IdCliente);
            ViewData["IdEmpleado"] = new SelectList(_context.Empleados, "IdEmpleado", "Cedula", renta.IdEmpleado);
            ViewData["IdVehiculo"] = new SelectList(_context.Vehiculos, "IdVehiculo", "Descripcion", renta.IdVehiculo);
            return View(renta);
        }

        // GET: Rentas/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var renta = await _context.Rentas.SingleOrDefaultAsync(m => m.IdRenta == id && m.Estado);
            if (renta == null)
            {
                return NotFound();
            }
            ViewData["IdCliente"] = new SelectList(_context.Clientes, "IdCliente", "Cedula", renta.IdCliente);
            ViewData["IdEmpleado"] = new SelectList(_context.Empleados, "IdEmpleado", "Cedula", renta.IdEmpleado);
            ViewData["IdVehiculo"] = new SelectList(_context.Vehiculos, "IdVehiculo", "Descripcion", renta.IdVehiculo);
            return View(renta);
        }

        // POST: Rentas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdRenta,FechaDevolucion,MontoDia,CantidadDias,Comentario,IdEmpleado,IdVehiculo,IdCliente")] Renta renta)
        {
            if (id != renta.IdRenta)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(renta);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RentaExists(renta.IdRenta))
                    {
                        return NotFound();
                    }

                    throw;
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdCliente"] = new SelectList(_context.Clientes, "IdCliente", "Cedula", renta.IdCliente);
            ViewData["IdEmpleado"] = new SelectList(_context.Empleados, "IdEmpleado", "Cedula", renta.IdEmpleado);
            ViewData["IdVehiculo"] = new SelectList(_context.Vehiculos, "IdVehiculo", "Descripcion", renta.IdVehiculo);
            return View(renta);
        }

        // GET: Rentas/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var renta = await _context.Rentas
                .Include(r => r.Cliente)
                .Include(r => r.Empleado)
                .Include(r => r.Vehiculo)
                .SingleOrDefaultAsync(m => m.IdRenta == id && m.Estado);
            if (renta == null)
            {
                return NotFound();
            }

            return View(renta);
        }

        // POST: Rentas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var renta = await _context.Rentas.SingleOrDefaultAsync(m => m.IdRenta == id);
            renta.Estado = false;
            _context.Rentas.Update(renta);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool RentaExists(int id)
        {
            return _context.Rentas.Any(e => e.IdRenta == id && e.Estado);
        }

        [HttpPost]
        public async Task<IActionResult> Devolver(int id)
        {
            var renta = await _context.Rentas.SingleOrDefaultAsync(m => m.IdRenta == id && m.Estado);
            if (renta.FechaDevolucion.HasValue && renta.FechaDevolucion.Value != default(DateTime))
                return RedirectToAction(nameof(Index));
            renta.FechaDevolucion = DateTime.Now;
            _context.Rentas.Update(renta);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
    }
}
