﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Syncfusion.EJ.ReportViewer;

namespace Web2.Controllers
{
    [Produces("application/json")]
    [Route("api/Report")]
    public class ReportController : Controller, IReportController
    {
        private readonly IMemoryCache _cache;

        private readonly IHostingEnvironment _hostingEnvironment;

        public ReportController(IMemoryCache memoryCache, 
            IHostingEnvironment hostingEnvironment)
        {
            _cache = memoryCache;
            _hostingEnvironment = hostingEnvironment;
        }

        public IActionResult Index()
        {
            return View();
        }
        
        public object PostReportAction([FromBody] Dictionary<string, object> jsonArray)
        {
            return ReportHelper.ProcessReport(jsonArray, this, _cache);
        }

        public void OnInitReportOptions(ReportViewerOptions reportOption)
        {
            var basePath = _hostingEnvironment.WebRootPath;
            var reportStream = new FileStream(basePath + @"\invoice.rdl", FileMode.Open, FileAccess.Read);
            reportOption.ReportModel.Stream = reportStream;
        }

        // Method will be called when reported is loaded with internally to start to layout process with ReportHelper.
        public void OnReportLoaded(ReportViewerOptions reportOption)
        {
        }

        public object PostFormReportAction()
        {
            throw new NotImplementedException();
        }

        //Get action for getting resources from the report
        [ActionName("GetResource")]
        [AcceptVerbs("GET")]
        // Method will be called from Report Viewer client to get the image src for Image report item.
        public object GetResource(ReportResource resource)
        {
            return ReportHelper.GetResource(resource, this, _cache);
        }
    }
}