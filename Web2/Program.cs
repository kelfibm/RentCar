﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Web2.Data;

namespace Web2
{
    public class Program
    {
        public static void Main(string[] args)
        {
            using (var context = new RentCarDbContext())
            {
                context.Database.EnsureCreated();
                new RentCarSeed().Seed(context);
                context.SaveChanges();
            }
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }
}
