﻿using FluentValidation;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RentCar.Model;

namespace Web2.Data
{
    public class TipoVehiculoConfiguration : IEntityTypeConfiguration<TipoVehiculo>
    {
        public void Configure(EntityTypeBuilder<TipoVehiculo> builder)
        {
            builder.ToTable("TipoVehiculo");
            builder.HasKey(m => m.IdTipoVehiculo);
            builder.Property(m => m.Descripcion).IsRequired().HasMaxLength(20);
            builder.Property(m => m.Estado).IsRequired();
        }
    }

    public class TipoVehiculoValidator : AbstractValidator<TipoVehiculo>
    {
        public TipoVehiculoValidator()
        {
            RuleFor(m => m.Descripcion).NotEmpty().MaximumLength(20);
        }
    }
}
