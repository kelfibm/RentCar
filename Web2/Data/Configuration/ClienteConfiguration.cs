﻿using FluentValidation;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RentCar.Helpers;
using RentCar.Model;

namespace Web2.Data
{
    public class ClienteConfiguration : IEntityTypeConfiguration<Cliente>
    {
        public void Configure(EntityTypeBuilder<Cliente> builder)
        {
            builder.ToTable("Cliente");
            builder.HasKey(m => m.IdCliente);
            builder.Property(m => m.Nombre).IsRequired().HasMaxLength(30);
            builder.Property(m => m.Cedula).IsRequired().HasMaxLength(11);
            builder.Property(m => m.NoTarjeta).IsRequired().HasMaxLength(16);
            builder.Property(m => m.LimiteCredito).IsRequired();

            builder.Property(m => m.IdTipoPersona).IsRequired();
            builder.HasOne(m => m.TipoPersona).WithMany(n => n.Clientes).HasForeignKey(m => m.IdTipoPersona);

            builder.Property(m => m.Estado).IsRequired();
        }
    }

    public class ClienteValidator : AbstractValidator<Cliente>
    {
        public ClienteValidator()
        {
            RuleFor(m => m.Nombre).NotEmpty().MaximumLength(30);
            RuleFor(m => m.Cedula).NotEmpty().Length(11).Must(j => Validator.IsValidCedula(j, true) || Validator.IsValidCedula(j, false)).WithMessage("Cédula invalida");
            RuleFor(m => m.NoTarjeta).NotEmpty().Length(16);
            RuleFor(m => m.LimiteCredito).NotEmpty();
            RuleFor(m => m.IdTipoPersona).NotEmpty().GreaterThan(0);
        }
    }
}
