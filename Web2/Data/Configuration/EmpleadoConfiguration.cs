﻿using FluentValidation;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RentCar.Helpers;
using RentCar.Model;

namespace Web2.Data
{
    public class EmpleadoConfiguration : IEntityTypeConfiguration<Empleado>
    {
        public void Configure(EntityTypeBuilder<Empleado> builder)
        {
            builder.ToTable("Empleado");
            builder.HasKey(m => m.IdEmpleado);
            builder.Property(m => m.Nombre).IsRequired().HasMaxLength(30);
            builder.Property(m => m.Cedula).IsRequired().HasMaxLength(11);
            builder.Property(m => m.Comision).IsRequired();
            builder.Property(m => m.FechaIngreso).IsRequired();
            
            builder.Property(m => m.IdTanda).IsRequired();
            builder.HasOne(m => m.Tanda).WithMany(n => n.Empleados).HasForeignKey(m => m.IdTanda);
            
            builder.Property(m => m.Estado).IsRequired();
        }
    }
    public class EmpleadoValidator : AbstractValidator<Empleado>
    {
        public EmpleadoValidator()
        {
            RuleFor(x => x.Nombre).NotEmpty().MaximumLength(30);
            RuleFor(x => x.Cedula).NotEmpty().Length(11).Must(j => Validator.IsValidCedula(j, true) || Validator.IsValidCedula(j, false)).WithMessage("Cédula invalida");
            RuleFor(x => x.Comision).NotEmpty();
            RuleFor(x => x.IdTanda).NotEmpty().GreaterThan(0);
        }
    }
}
