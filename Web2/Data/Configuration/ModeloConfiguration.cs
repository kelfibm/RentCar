﻿using FluentValidation;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RentCar.Model;

namespace Web2.Data
{
    public class ModeloConfiguration : IEntityTypeConfiguration<Modelo>
    {
        public void Configure(EntityTypeBuilder<Modelo> builder)
        {
            builder.ToTable("Modelo");
            builder.HasKey(m => m.IdModelo);
            builder.Property(m => m.Descripcion).IsRequired().HasMaxLength(30);
            
            builder.Property(m => m.IdMarca).IsRequired();
            builder.HasOne(m => m.Marca).WithMany(n => n.Modelos).HasForeignKey(m => m.IdMarca);
            
            builder.Property(m => m.Estado).IsRequired();
        }
    }

    public class ModeloValidator : AbstractValidator<Modelo>
    {
        public ModeloValidator()
        {
            RuleFor(m => m.Descripcion).NotEmpty().MaximumLength(30);
            RuleFor(m => m.IdMarca).NotEmpty().GreaterThan(0);
        }
    }
}
