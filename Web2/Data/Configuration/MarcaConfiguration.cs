﻿using FluentValidation;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RentCar.Model;

namespace Web2.Data
{
    public class MarcaConfiguration : IEntityTypeConfiguration<Marca>
    {
        public void Configure(EntityTypeBuilder<Marca> builder)
        {
            builder.ToTable("Marca");
            builder.HasKey(m => m.IdMarca);
            builder.Property(m => m.Descripcion).IsRequired().HasMaxLength(30);
            builder.Property(m => m.Estado).IsRequired();
        }
    }

    public class MarcaValidator : AbstractValidator<Marca>
    {
        public MarcaValidator()
        {
            RuleFor(m => m.Descripcion).NotEmpty().MaximumLength(30);
        }
    }
}
