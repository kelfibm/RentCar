﻿using FluentValidation;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RentCar.Model;

namespace Web2.Data
{
    public class InspeccionConfiguration : IEntityTypeConfiguration<Inspeccion>
    {
        public void Configure(EntityTypeBuilder<Inspeccion> builder)
        {
            builder.ToTable("Inspeccion");
            builder.HasKey(m => m.IdInspeccion);
            builder.Property(m => m.Ralladuras).IsRequired();
            builder.Property(m => m.CantidadCombustible).IsRequired();
            builder.Property(m => m.GomaRespuesto).IsRequired();
            builder.Property(m => m.Gato).IsRequired();
            builder.Property(m => m.Roturas).IsRequired();
            builder.Property(m => m.GomasEstables).IsRequired();
            builder.Property(m => m.Observaciones).IsRequired(false).HasMaxLength(500);
            builder.Property(m => m.Fecha).IsRequired();
            
            builder.Property(m => m.IdVehiculo).IsRequired();
            builder.HasOne(m => m.Vehiculo).WithMany(n => n.Inspecciones).HasForeignKey(m => m.IdVehiculo);
            
            builder.Property(m => m.IdCliente).IsRequired();
            builder.HasOne(m => m.Cliente).WithMany(n => n.Inspecciones).HasForeignKey(m => m.IdCliente);
            
            builder.Property(m => m.IdEmpleado).IsRequired();
            builder.HasOne(m => m.Empleado).WithMany(n => n.Inspecciones).HasForeignKey(m => m.IdEmpleado);
            
            builder.Property(m => m.Estado).IsRequired();
        }
    }

    public class InspeccionValidator : AbstractValidator<Inspeccion>
    {
        public InspeccionValidator()
        {
            RuleFor(m => m.CantidadCombustible).NotEmpty().GreaterThanOrEqualTo(0);
            RuleFor(m => m.Observaciones).MaximumLength(500);
            RuleFor(m => m.IdVehiculo).NotEmpty().GreaterThan(0);
            RuleFor(m => m.IdCliente).NotEmpty().GreaterThan(0);
            RuleFor(m => m.IdEmpleado).NotEmpty().GreaterThan(0);
        }
    }
}
