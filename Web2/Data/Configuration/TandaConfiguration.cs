﻿using FluentValidation;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RentCar.Model;

namespace Web2.Data
{
    public class TandaConfiguration : IEntityTypeConfiguration<Tanda>
    {
        public void Configure(EntityTypeBuilder<Tanda> builder)
        {
            builder.ToTable("Tanda");
            builder.HasKey(m => m.IdTanda);
            builder.Property(m => m.Descripcion).IsRequired().HasMaxLength(20);
            builder.Property(m => m.Estado).IsRequired();
        }
    }

    public class TandaValidator : AbstractValidator<Tanda>
    {
        public TandaValidator()
        {
            RuleFor(m => m.Descripcion).NotEmpty().MaximumLength(20);
        }
    }
}
