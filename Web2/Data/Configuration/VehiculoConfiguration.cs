﻿using FluentValidation;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RentCar.Model;

namespace Web2.Data
{
    public class VehiculoConfiguration : IEntityTypeConfiguration<Vehiculo>
    {
        public void Configure(EntityTypeBuilder<Vehiculo> builder)
        {
            builder.ToTable("Vehiculo");
            builder.HasKey(m => m.IdVehiculo);
            builder.Property(m => m.Descripcion).IsRequired().HasMaxLength(30);
            builder.Property(m => m.NoChasis).IsRequired().HasMaxLength(30);
            builder.Property(m => m.NoMotor).IsRequired().HasMaxLength(30);
            builder.Property(m => m.NoPlaca).IsRequired().HasMaxLength(30);
            
            builder.Property(m => m.IdTipoVehiculo).IsRequired();
            builder.HasOne(m => m.TipoVehiculo).WithMany(n => n.Vehiculos).HasForeignKey(m => m.IdTipoVehiculo);
            
            builder.Property(m => m.IdModelo).IsRequired();
            builder.HasOne(m => m.Modelo).WithMany(n => n.Vehiculos).HasForeignKey(m => m.IdModelo);
            
            builder.Property(m => m.IdTipoCombustible).IsRequired();
            builder.HasOne(m => m.TipoCombustible).WithMany(n => n.Vehiculos).HasForeignKey(m => m.IdTipoCombustible);
            
            builder.Property(m => m.Estado).IsRequired();
        }
    }

    public class VehiculoValidator : AbstractValidator<Vehiculo>
    {
        public VehiculoValidator()
        {
            RuleFor(m => m.Descripcion).NotEmpty().MaximumLength(30);
            RuleFor(m => m.NoChasis).NotEmpty().MaximumLength(30);
            RuleFor(m => m.NoMotor).NotEmpty().MaximumLength(30);
            RuleFor(m => m.NoPlaca).NotEmpty().MaximumLength(30);
            RuleFor(m => m.IdTipoVehiculo).NotEmpty().GreaterThan(0);
            RuleFor(m => m.IdModelo).NotEmpty().GreaterThan(0);
            RuleFor(m => m.IdTipoCombustible).NotEmpty().GreaterThan(0);
        }
    }
}
