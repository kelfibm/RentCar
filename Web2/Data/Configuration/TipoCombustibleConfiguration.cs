﻿using FluentValidation;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RentCar.Model;

namespace Web2.Data
{
    public class TipoCombustibleConfiguration : IEntityTypeConfiguration<TipoCombustible>
    {
        public void Configure(EntityTypeBuilder<TipoCombustible> builder)
        {
            builder.ToTable("TipoCombustible");
            builder.HasKey(m => m.IdTipoCombustible);
            builder.Property(m => m.Descripcion).IsRequired().HasMaxLength(20);
            builder.Property(m => m.Estado).IsRequired();
        }
    }

    public class TipoCombustibleValidator : AbstractValidator<TipoCombustible>
    {
        public TipoCombustibleValidator()
        {
            RuleFor(m => m.Descripcion).NotEmpty().MaximumLength(20);
        }
    }
}
