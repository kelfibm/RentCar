﻿using FluentValidation;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RentCar.Model;

namespace Web2.Data
{
    public class TipoPersonaConfiguration : IEntityTypeConfiguration<TipoPersona>
    {
        public void Configure(EntityTypeBuilder<TipoPersona> builder)
        {
            builder.ToTable("TipoPersona");
            builder.HasKey(m => m.IdTipoPersona);
            builder.Property(m => m.Descripcion).IsRequired().HasMaxLength(20);
            builder.Property(m => m.Estado).IsRequired();
        }
    }

    public class TipoPersonaValidator : AbstractValidator<TipoPersona>
    {
        public TipoPersonaValidator()
        {
            RuleFor(m => m.Descripcion).NotEmpty().MaximumLength(20);
        }
    }
}
