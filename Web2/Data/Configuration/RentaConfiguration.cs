﻿using System.Linq;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using RentCar.Model;

namespace Web2.Data
{
    public class RentaConfiguration: IEntityTypeConfiguration<Renta>
    {
        public void Configure(EntityTypeBuilder<Renta> builder)
        {
            builder.ToTable("Renta");
            builder.HasKey(m => m.IdRenta);
            builder.Property(m => m.FechaRenta).IsRequired();
            builder.Property(m => m.FechaDevolucion).IsRequired(false);
            builder.Property(m => m.MontoDia).IsRequired();
            builder.Property(m => m.CantidadDias).IsRequired();
            builder.Property(m => m.Comentario).IsRequired().HasMaxLength(100);

            builder.Property(m => m.IdEmpleado).IsRequired();
            builder.HasOne(m => m.Empleado).WithMany(n => n.Rentas).HasForeignKey(m => m.IdEmpleado);

            builder.Property(m => m.IdVehiculo).IsRequired();
            builder.HasOne(m => m.Vehiculo).WithMany(n => n.Rentas).HasForeignKey(m => m.IdVehiculo);

            builder.Property(m => m.IdCliente).IsRequired();
            builder.HasOne(m => m.Cliente).WithMany(n => n.Rentas).HasForeignKey(m => m.IdCliente);
            
            builder.Property(m => m.Estado).IsRequired();
        }
    }

    public class RentaValidator : AbstractValidator<Renta>
    {
        public RentaValidator()
        {
            RuleFor(m => m.FechaDevolucion).GreaterThan(m => m.FechaRenta);
            RuleFor(m => m.MontoDia).NotEmpty().GreaterThan(0);
            RuleFor(m => m.CantidadDias).NotEmpty().GreaterThan(0);
            RuleFor(m => m.Comentario).NotEmpty().MaximumLength(100);
            RuleFor(m => m.IdEmpleado).NotEmpty().GreaterThan(0);
            RuleFor(m => m.IdVehiculo).NotEmpty().GreaterThan(0).Custom((prop, context) =>
            {
                using (var dbcontext = new RentCarDbContext())
                {
                    var vehiculo = dbcontext.Vehiculos.SingleOrDefault(x => x.IdVehiculo == prop && x.Estado);
                    if (vehiculo == null)
                    {
                        context.AddFailure("El vehículo no existe");
                        return;
                    }
                    if (!vehiculo.Inspeccionado)
                    {
                        context.AddFailure("El vehículo debe ser inspeccionado antes de ser rentado");
                    }
                }
            });
            RuleFor(m => m.IdCliente).NotEmpty().GreaterThan(0);
        }
    }
}
