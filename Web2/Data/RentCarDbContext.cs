﻿using Microsoft.EntityFrameworkCore;
using RentCar.Model;

namespace Web2.Data
{
    public class RentCarDbContext : DbContext
    {
        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Empleado> Empleados { get; set; }
        public DbSet<Inspeccion> Inspecciones { get; set; }
        public DbSet<Marca> Marcas { get; set; }
        public DbSet<Modelo> Modelos { get; set; }
        public DbSet<Renta> Rentas { get; set; }
        public DbSet<Tanda> Tandas { get; set; }
        public DbSet<TipoCombustible> TipoCombustibles { get; set; }
        public DbSet<TipoPersona> TipoPersonas { get; set; }
        public DbSet<TipoVehiculo> TipoVehiculos { get; set; }
        public DbSet<Vehiculo> Vehiculos { get; set; }

        /*public RentCarDbContext(DbContextOptions opt) : base(opt)
        {
        }*/

        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySQL("server=topodata.com;database=topodata_rentCarOwn;user=topod_topoStudy;password=Topo.1953;SslMode=none");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new ClienteConfiguration());
            modelBuilder.ApplyConfiguration(new EmpleadoConfiguration());
            modelBuilder.ApplyConfiguration(new InspeccionConfiguration());
            modelBuilder.ApplyConfiguration(new MarcaConfiguration());
            modelBuilder.ApplyConfiguration(new ModeloConfiguration());
            modelBuilder.ApplyConfiguration(new RentaConfiguration());
            modelBuilder.ApplyConfiguration(new TandaConfiguration());
            modelBuilder.ApplyConfiguration(new TipoCombustibleConfiguration());
            modelBuilder.ApplyConfiguration(new TipoPersonaConfiguration());
            modelBuilder.ApplyConfiguration(new TipoVehiculoConfiguration());
            modelBuilder.ApplyConfiguration(new VehiculoConfiguration());

            /*var seed = new RentCarSeed();
            modelBuilder.Entity<TipoPersona>().HasData(seed.TipoPersonas);
            modelBuilder.Entity<Tanda>().HasData(seed.Tandas);
            modelBuilder.Entity<TipoVehiculo>().HasData(seed.TipoVehiculos);
            modelBuilder.Entity<TipoCombustible>().HasData(seed.TipoCombustibles);*/
        }
    }
}
