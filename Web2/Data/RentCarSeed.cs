﻿using System;
using System.Collections.Generic;
using System.Linq;
using RentCar.Model;

namespace Web2.Data
{
    public class RentCarSeed
    {
        public void Seed(RentCarDbContext context)
        {
            if (!context.TipoPersonas.Any())
            {
                context.TipoPersonas.AddRange(TipoPersonas);
            }

            if (!context.Tandas.Any())
            {
                context.Tandas.AddRange(Tandas);
            }

            if (!context.TipoVehiculos.Any())
            {
                context.TipoVehiculos.AddRange(TipoVehiculos);
            }

            if (!context.TipoCombustibles.Any())
            {
                context.TipoCombustibles.AddRange(TipoCombustibles);
            }

            if (!context.Empleados.Any())
            {
                context.Empleados.AddRange(Empleados);
            }
            if (!context.Marcas.Any())
            {
                context.Marcas.AddRange(Marcas);
            }
            if (!context.Modelos.Any())
            {
                context.Modelos.AddRange(Modelos);
            }
            if (!context.Clientes.Any())
            {
                context.Clientes.AddRange(Clientes);
            }
            context.SaveChanges();
        }

        public IEnumerable<TipoPersona> TipoPersonas => new List<TipoPersona>
        {
            new TipoPersona
            {
                Descripcion = "Fisica",
                Estado = true
            },
            new TipoPersona
            {
                Descripcion = "Juridica",
                Estado = true
            }
        };

        public IEnumerable<Tanda> Tandas => new List<Tanda>
            {
                new Tanda
                {
                    Descripcion = "Matutina",
                    Estado = true
                },
                new Tanda
                {
                    Descripcion = "Vespertina",
                    Estado = true
                },
                new Tanda
                {
                    Descripcion = "Nocturna",
                    Estado = true
                }
            };

        public IEnumerable<TipoVehiculo> TipoVehiculos => new List<TipoVehiculo>
            {
                new TipoVehiculo
                {
                    Descripcion = "Motocicleta",
                    Estado = true
                },
                new TipoVehiculo
                {
                    Descripcion = "Carro",
                    Estado = true
                },
                new TipoVehiculo
                {
                    Descripcion = "Jeep",
                    Estado = true
                },
                new TipoVehiculo
                {
                    Descripcion = "Camion",
                    Estado = true
                }
            };

        public IEnumerable<TipoCombustible> TipoCombustibles => new List<TipoCombustible>
            {
                new TipoCombustible
                {
                    Descripcion = "Gasolina",
                    Estado = true
                },
                new TipoCombustible
                {
                    Descripcion = "Gas Oil",
                    Estado = true
                },
                new TipoCombustible
                {
                    Descripcion = "Gas Natural",
                    Estado = true
                }
            };

        public IEnumerable<Empleado> Empleados => new List<Empleado>
        {
            new Empleado
            {
                Cedula = "40200711493",
                Comision = 5,
                FechaIngreso = DateTime.Now,
                IdTanda = 1,
                Nombre = "Kelfi Batista",
            }
        };

        public IEnumerable<Marca> Marcas => new List<Marca>
        {
            new Marca
            {
                Descripcion = "Toyota"
            },
            new Marca
            {
                Descripcion = "Honda"
            }
        };

        public IEnumerable<Modelo> Modelos => new List<Modelo>
        {
            new Modelo
            {
                Descripcion = "Modelo 1",
                IdMarca = 1
            },
            new Modelo
            {
                Descripcion = "Civic",
                IdMarca = 2
            }
        };
        public IEnumerable<Cliente> Clientes => new List<Cliente>
        {
            new Cliente
            {
                Cedula = "40215276938",
                IdTipoPersona = 1,
                LimiteCredito = 500,
                NoTarjeta = "12345678901234",
                Nombre = "Darlenys"
            }
        };
    }
}
