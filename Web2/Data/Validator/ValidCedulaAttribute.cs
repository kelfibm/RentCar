﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Web2.Data
{
    public class ValidCedulaAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            var cedula = value as string;
            return RentCar.Helpers.Validator.IsValidCedula(cedula,true) || RentCar.Helpers.Validator.IsValidCedula(cedula, false);
        }
    }
}
