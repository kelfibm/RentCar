﻿using System;
using System.Linq;

namespace RentCar.Helpers
{
    public class Validator
    {
        public bool HasEmptyStrings(params string[] listaCadena)
        {
            return listaCadena.Any(cadena => cadena.Trim() == string.Empty);
        }

        public static bool IsValidCedula(string cedula, bool hasSymbols)
        {
            var cedulaParts = new string[3];
            string municipio;
            string digitoVerificador;

            if (hasSymbols)
            {
                if (cedula.Length < 13)
                {
                    return false;
                }

                cedulaParts = cedula.Split('-');
                municipio = cedulaParts[0].Replace(" ", string.Empty) + cedulaParts[1].Replace(" ", string.Empty);
                digitoVerificador = cedulaParts[2].Replace(" ", string.Empty);

                try
                {
                    Convert.ToInt32(digitoVerificador);
                }
                catch (FormatException)
                {
                    return false;
                }
            }
            else
            {
                if (cedula.Length < 11)
                {
                    return false;
                }

                cedulaParts[0] = cedula.Substring(0, 3);
                cedulaParts[1] = cedula.Substring(3, 7);
                cedulaParts[2] = cedula.Substring(10, 1);

                municipio = cedulaParts[0] + cedulaParts[1];
                digitoVerificador = cedulaParts[2];

                try
                {
                    Convert.ToInt32(digitoVerificador);
                }
                catch (Exception)
                {
                    return false;
                }
            }

            var suma = 0;

            for (var i = 0; i < municipio.Length; i++)
            {
                var mod = (i % 2) == 0 ? "1" : "2";

                string val;

                try
                {
                    var a = municipio.Substring(i, 1);
                    Convert.ToInt32(a);
                    val = a;
                }
                catch (Exception)
                {
                    return false;
                }
                
                var res = Convert.ToInt32(Convert.ToInt32(val) * Convert.ToInt32(mod));

                if (res > 9)
                {
                    var res1 = res.ToString();
                    var uno = res1.Substring(0, 1);
                    var dos = res1.Substring(1, 1);
                    res = Convert.ToInt32(uno) + Convert.ToInt32(dos);
                }

                suma += res;
            }

            var elNumero = (10 - (suma % 10) % 10);

            return elNumero == Convert.ToInt32(digitoVerificador) && cedulaParts[0] != "000";
        }
    }
}
